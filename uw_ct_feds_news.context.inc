<?php

/**
 * @file
 * uw_ct_feds_news.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_feds_news_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_advocacy_related_news';
  $context->description = '';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'advocacy' => 'advocacy',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-41b977002e655a009af5892891a1304f' => array(
          'module' => 'views',
          'delta' => '41b977002e655a009af5892891a1304f',
          'region' => 'secondary_content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  $export['feds_advocacy_related_news'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_homepage_news';
  $context->description = 'Displays news item block on a site\'s front page.';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-8bd823d5e9175d6dca9b791045fb0fca' => array(
          'module' => 'views',
          'delta' => '8bd823d5e9175d6dca9b791045fb0fca',
          'region' => 'feature_news',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Displays news item block on a site\'s front page.');
  t('Feds');
  $export['feds_homepage_news'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_news_listing';
  $context->description = 'Feds News Listing Page';
  $context->tag = 'Feds';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'feds_news:feds_news_listing_page' => 'feds_news:feds_news_listing_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-a3e4198a20529b4169284a2e4417756c' => array(
          'module' => 'views',
          'delta' => 'a3e4198a20529b4169284a2e4417756c',
          'region' => 'featured_top',
          'weight' => '-8',
        ),
        'views-0706636c1c11786379fe7ccf24f5baf1' => array(
          'module' => 'views',
          'delta' => '0706636c1c11786379fe7ccf24f5baf1',
          'region' => 'secondary_content',
          'weight' => '-15',
        ),
        'views-2e19d00ad2bbcc32cb95a3afc9b8b95c' => array(
          'module' => 'views',
          'delta' => '2e19d00ad2bbcc32cb95a3afc9b8b95c',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-f8eeddf373795a58ceaa77f6d3071b3c' => array(
          'module' => 'views',
          'delta' => 'f8eeddf373795a58ceaa77f6d3071b3c',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  t('Feds News Listing Page');
  $export['feds_news_listing'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_news_sidebar';
  $context->description = 'Views displayed on the sidebar within the news section.';
  $context->tag = 'Feds';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'feds_news' => 'feds_news',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'views' => array(
      'values' => array(
        'feds_news:feds_news_taxonomy_filter_page' => 'feds_news:feds_news_taxonomy_filter_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-0706636c1c11786379fe7ccf24f5baf1' => array(
          'module' => 'views',
          'delta' => '0706636c1c11786379fe7ccf24f5baf1',
          'region' => 'secondary_content',
          'weight' => '-10',
        ),
        'views-2e19d00ad2bbcc32cb95a3afc9b8b95c' => array(
          'module' => 'views',
          'delta' => '2e19d00ad2bbcc32cb95a3afc9b8b95c',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-f8eeddf373795a58ceaa77f6d3071b3c' => array(
          'module' => 'views',
          'delta' => 'f8eeddf373795a58ceaa77f6d3071b3c',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  t('Views displayed on the sidebar within the news section.');
  $export['feds_news_sidebar'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_social_media_sharing';
  $context->description = 'Display social media block on only News article pages';
  $context->tag = 'Feds';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'feds_news' => 'feds_news',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'node/*' => 'node/*',
        'news-updates/*' => 'news-updates/*',
        '~news-updates/' => '~news-updates/',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_social_media_sharing-social_media_block' => array(
          'module' => 'uw_social_media_sharing',
          'delta' => 'social_media_block',
          'region' => 'secondary_content',
          'weight' => '-63',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Display social media block on only News article pages');
  t('Feds');
  $export['feds_social_media_sharing'] = $context;

  return $export;
}
