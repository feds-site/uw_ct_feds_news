<?php

/**
 * @file
 * uw_ct_feds_news.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function uw_ct_feds_news_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:feds_news.
  $config['node:feds_news'] = array(
    'instance' => 'node:feds_news',
    'disabled' => FALSE,
    'config' => array(
      'description' => array(
        'value' => '[node:field_author_full_name] - [node:summary]
    ',
      ),
      'robots' => array(
        'value' => array(
          'index' => 0,
          'follow' => 0,
          'noindex' => 0,
          'nofollow' => 0,
          'noarchive' => 0,
          'nosnippet' => 0,
          'noodp' => 0,
          'noydir' => 0,
          'noimageindex' => 0,
          'notranslate' => 0,
        ),
      ),
      'image_src' => array(
        'value' => 'https://wusa.ca/sites/ca.feds/files/uploads/images/feds_logo_social_sqaure.jpg',
      ),
      'canonical' => array(
        'value' => 'https://wusa.ca/[current-page:url:args:join-path]',
      ),
      'shortlink' => array(
        'value' => 'https://wusa.ca/[current-page:url:args:join-path]',
      ),
      'og:url' => array(
        'value' => 'https://wusa.ca/[current-page:url:args:join-path]',
      ),
      'og:image' => array(
        'value' => 'https://wusa.ca/sites/ca.feds/themes/feds_theme/images/wusa-logo-152.png',
      ),
      'twitter:image' => array(
        'value' => '[node:field_feds_news_feature_image]',
      ),
    ),
  );

  return $config;
}
