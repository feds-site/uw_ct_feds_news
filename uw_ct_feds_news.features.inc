<?php

/**
 * @file
 * uw_ct_feds_news.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_feds_news_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_feds_news_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_field_default_field_instances_alter().
 */
function uw_ct_feds_news_field_default_field_instances_alter(&$data) {
  if (isset($data['node-feds_news-field_image_to_body'])) {
    $data['node-feds_news-field_image_to_body']['settings']['image_field_caption_wrapper']['image_field_caption_default']['value'] = '<p>Caption: here is my default caption text</p>
    '; /* WAS: '<p>Caption: here is my default caption text</p>
    ' */
  }
}

/**
 * Implements hook_metatag_export_default_alter().
 */
function uw_ct_feds_news_metatag_export_default_alter(&$data) {
  if (isset($data['global'])) {
    $data['global']['config']['canonical']['value'] = 'https://wusa.ca/[current-page:url:args:join-path]'; /* WAS: '[current-page:url:absolute]' */
  }
}

/**
 * Implements hook_user_default_permissions_alter().
 */
function uw_ct_feds_news_user_default_permissions_alter(&$data) {
  if (isset($data['edit meta tag: canonical'])) {
    $data['edit meta tag: canonical']['roles']['site manager'] = 'site manager'; /* WAS: '' */
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_ct_feds_news_strongarm_alter(&$data) {
  if (isset($data['default_metatag_config'])) {
    $data['default_metatag_config']->value['node:feds_news'] = FALSE; /* WAS: '' */
    $data['default_metatag_config']->value['node:uw_ct_feds_news'] = FALSE; /* WAS: '' */
    $data['default_metatag_config']->value['node:uw_stories'] = FALSE; /* WAS: '' */
  }
  if (isset($data['metatag_enable_node__uw_ct_feds_news'])) {
    unset($data['metatag_enable_node__uw_ct_feds_news']);
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_feds_news_node_info() {
  $items = array(
    'feds_news' => array(
      'name' => t('Feds news'),
      'base' => 'node_content',
      'description' => t('Feds news articles and important information.'),
      'has_title' => '1',
      'title_label' => t('News'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_ct_feds_news_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: feds_news
  $schemaorg['node']['feds_news'] = array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'field_image_to_body' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_news_banner_image' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_feds_news_category' => array(
      'predicates' => array(),
    ),
    'field_author_full_name' => array(
      'predicates' => array(),
    ),
    'field_author_title_position' => array(
      'predicates' => array(),
    ),
    'field_authors_photo' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_news_categories' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_news_taxonomy' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_spotlight_name' => array(
      'predicates' => array(),
    ),
    'field_spotlight_position' => array(
      'predicates' => array(),
    ),
    'field_feds_news_feature_image' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_thumbnail_image' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_content_is_a_spotlight' => array(
      'predicates' => array(),
    ),
    'field_term_reference' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_news_category_spotlight' => array(
      'predicates' => array(),
    ),
    'field_spotlight_heading' => array(
      'predicates' => array(),
    ),
    'field_spotlight_featured_aside' => array(
      'predicates' => array(),
    ),
    'field_feature_article' => array(
      'predicates' => array(),
    ),
  );

  // Exported RDF mapping: feds_news_categories
  $schemaorg['taxonomy_term']['feds_news_categories'] = array(
    'rdftype' => array(
      0 => 'skos:Concept',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'rdfs:label',
        1 => 'skos:prefLabel',
      ),
    ),
    'description' => array(
      'predicates' => array(
        0 => 'skos:definition',
      ),
    ),
    'vid' => array(
      'predicates' => array(
        0 => 'skos:inScheme',
      ),
      'type' => 'rel',
    ),
    'parent' => array(
      'predicates' => array(
        0 => 'skos:broader',
      ),
      'type' => 'rel',
    ),
  );

  return $schemaorg;
}
