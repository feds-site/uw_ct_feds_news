<?php

/**
 * @file
 * uw_ct_feds_news.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_feds_news_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_instance
  $overrides["field_instance.node-feds_news-field_image_to_body.settings|image_field_caption_wrapper|image_field_caption_default|value"] = '<p>Caption: here is my default caption text</p>
  ';

  // Exported overrides for: metatag
  $overrides["metatag.global.config|canonical|value"] = 'https://wusa.ca/[current-page:url:args:join-path]';

  // Exported overrides for: user_permission
  $overrides["user_permission.edit meta tag: canonical.roles|site manager"] = 'site manager';

  // Exported overrides for: variable
  $overrides["variable.default_metatag_config.value|node:feds_news"] = FALSE;
  $overrides["variable.default_metatag_config.value|node:uw_ct_feds_news"] = FALSE;
  $overrides["variable.default_metatag_config.value|node:uw_stories"] = FALSE;
  $overrides["variable.metatag_enable_node__uw_ct_feds_news.metatag_enable_node__uw_ct_feds_news"]["DELETED"] = TRUE;

 return $overrides;
}
