<?php

/**
 * @file
 * uw_ct_feds_news.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_ct_feds_news_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Elections and Referendums',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 5,
    'uuid' => '1b705ec7-eb27-4bf4-bdda-34b9565d2223',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'und' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
        'image_src' => array(
          'value' => '',
        ),
        'canonical' => array(
          'value' => '',
        ),
        'shortlink' => array(
          'value' => '',
        ),
        'og:url' => array(
          'value' => '',
        ),
        'og:title' => array(
          'value' => '',
        ),
        'og:image' => array(
          'value' => '',
        ),
        'twitter:url' => array(
          'value' => '',
        ),
        'twitter:title' => array(
          'value' => '',
        ),
        'twitter:image' => array(
          'value' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/127',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'General Meeting',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 8,
    'uuid' => '1cf6dfad-8b69-49fe-b82f-1747502d4cc3',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'und' => array(
        'image_src' => array(
          'value' => '',
        ),
        'canonical' => array(
          'value' => '',
        ),
        'shortlink' => array(
          'value' => '',
        ),
        'og:url' => array(
          'value' => '',
        ),
        'og:title' => array(
          'value' => '',
        ),
        'twitter:url' => array(
          'value' => '',
        ),
        'twitter:title' => array(
          'value' => '',
        ),
        'twitter:image' => array(
          'value' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/128',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Advocacy',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 11,
    'uuid' => '36395dd3-d796-4aad-beee-9557d82e9d0e',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'und' => array(
        'image_src' => array(
          'value' => '',
        ),
        'canonical' => array(
          'value' => '',
        ),
        'shortlink' => array(
          'value' => '',
        ),
        'og:image' => array(
          'value' => '',
        ),
        'twitter:url' => array(
          'value' => '',
        ),
        'twitter:title' => array(
          'value' => '',
        ),
        'twitter:image' => array(
          'value' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/122',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Services',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 10,
    'uuid' => '3919d60b-f454-4e42-a4f2-44ddf7891e26',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'und' => array(
        'image_src' => array(
          'value' => '',
        ),
        'canonical' => array(
          'value' => '',
        ),
        'shortlink' => array(
          'value' => '',
        ),
        'og:url' => array(
          'value' => '',
        ),
        'og:title' => array(
          'value' => '',
        ),
        'og:image' => array(
          'value' => '',
        ),
        'twitter:url' => array(
          'value' => '',
        ),
        'twitter:image' => array(
          'value' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/125',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Wellness Days',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => '4c53934e-c333-42f9-9c0d-b5646674c589',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'und' => array(
        'image_src' => array(
          'value' => '',
        ),
        'canonical' => array(
          'value' => '',
        ),
        'shortlink' => array(
          'value' => '',
        ),
        'og:url' => array(
          'value' => '',
        ),
        'og:image' => array(
          'value' => '',
        ),
        'twitter:url' => array(
          'value' => '',
        ),
        'twitter:image' => array(
          'value' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/[term:tid]',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'General',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 12,
    'uuid' => '51813f30-0def-4c41-9cf3-3b1dcf2d46f6',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/140',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Societies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 9,
    'uuid' => '5d57ffe8-35ca-46aa-9512-1c6a05c39c01',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'und' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
        'image_src' => array(
          'value' => '',
        ),
        'canonical' => array(
          'value' => '',
        ),
        'shortlink' => array(
          'value' => '',
        ),
        'og:url' => array(
          'value' => '',
        ),
        'og:title' => array(
          'value' => '',
        ),
        'og:image' => array(
          'value' => '',
        ),
        'twitter:image' => array(
          'value' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/126',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Tips and Tricks',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 4,
    'uuid' => '5f130c43-7a3f-454d-8aaa-1af184dbca33',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/139',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Governance',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 14,
    'uuid' => '7b74feb4-7fd7-4c98-a376-a429a837a92a',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/137',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Welcome Week',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 2,
    'uuid' => '7faa6091-d3b7-4e1a-a515-82335c507fcd',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'und' => array(
        'image_src' => array(
          'value' => '',
        ),
        'canonical' => array(
          'value' => '',
        ),
        'shortlink' => array(
          'value' => '',
        ),
        'og:url' => array(
          'value' => '',
        ),
        'og:image' => array(
          'value' => '',
        ),
        'twitter:url' => array(
          'value' => '',
        ),
        'twitter:title' => array(
          'value' => '',
        ),
        'twitter:image' => array(
          'value' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/129',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Orientation',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 13,
    'uuid' => '9bb809fa-25db-440a-9d9a-d8583aecf29c',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/138',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Featured',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'a221648d-b98c-47b5-8a60-ce3e90442967',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/136',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Clubs',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 6,
    'uuid' => 'a2334591-d230-4591-8183-cfaf34e37edd',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'und' => array(
        'image_src' => array(
          'value' => '',
        ),
        'canonical' => array(
          'value' => '',
        ),
        'shortlink' => array(
          'value' => '',
        ),
        'og:url' => array(
          'value' => '',
        ),
        'og:image' => array(
          'value' => '',
        ),
        'twitter:url' => array(
          'value' => '',
        ),
        'twitter:image' => array(
          'value' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/124',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Events',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 7,
    'uuid' => 'ddb461b0-6da4-48a1-8a1e-4506a46c1758',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'und' => array(
        'image_src' => array(
          'value' => '',
        ),
        'canonical' => array(
          'value' => '',
        ),
        'shortlink' => array(
          'value' => '',
        ),
        'og:url' => array(
          'value' => '',
        ),
        'og:title' => array(
          'value' => '',
        ),
        'og:image' => array(
          'value' => '',
        ),
        'twitter:url' => array(
          'value' => '',
        ),
        'twitter:title' => array(
          'value' => '',
        ),
        'twitter:image' => array(
          'value' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/123',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Spotlight',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 3,
    'uuid' => 'f6a9c7a3-60b8-4484-9456-0cfcc4e67e7a',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_news_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'und' => array(
        'image_src' => array(
          'value' => '',
        ),
        'canonical' => array(
          'value' => '',
        ),
        'shortlink' => array(
          'value' => '',
        ),
        'og:url' => array(
          'value' => '',
        ),
        'og:image' => array(
          'value' => '',
        ),
        'twitter:url' => array(
          'value' => '',
        ),
        'twitter:image' => array(
          'value' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'news-updates/term/130',
        'language' => 'und',
      ),
    ),
  );
  return $terms;
}
