<?php
/**
 * @file
 * uw_ct_feds_news.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_feds_news_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_metatag:admin/config/search/metatags.
  $menu_links['management_metatag:admin/config/search/metatags'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/search/metatags',
    'router_path' => 'admin/config/search/metatags',
    'link_title' => 'Metatag',
    'options' => array(
      'attributes' => array(
        'title' => 'Configure Metatag defaults.',
      ),
      'alter' => TRUE,
      'identifier' => 'management_metatag:admin/config/search/metatags',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
    'parent_identifier' => 'management_search-and-metadata:admin/config/search',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Metatag');

  return $menu_links;
}
