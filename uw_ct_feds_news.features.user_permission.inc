<?php

/**
 * @file
 * uw_ct_feds_news.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_news_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'context ajax block access'.
  $permissions['context ajax block access'] = array(
    'name' => 'context ajax block access',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'context_ui',
  );

  // Exported permission: 'create feds_news content'.
  $permissions['create feds_news content'] = array(
    'name' => 'create feds_news content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'define view for terms in feds_news_categories'.
  $permissions['define view for terms in feds_news_categories'] = array(
    'name' => 'define view for terms in feds_news_categories',
    'roles' => array(),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for vocabulary feds_news_categories'.
  $permissions['define view for vocabulary feds_news_categories'] = array(
    'name' => 'define view for vocabulary feds_news_categories',
    'roles' => array(),
    'module' => 'tvi',
  );

  // Exported permission: 'delete any feds_news content'.
  $permissions['delete any feds_news content'] = array(
    'name' => 'delete any feds_news content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_news content'.
  $permissions['delete own feds_news content'] = array(
    'name' => 'delete own feds_news content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in feds_news_categories'.
  $permissions['delete terms in feds_news_categories'] = array(
    'name' => 'delete terms in feds_news_categories',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any feds_news content'.
  $permissions['edit any feds_news content'] = array(
    'name' => 'edit any feds_news content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_news content'.
  $permissions['edit own feds_news content'] = array(
    'name' => 'edit own feds_news content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in feds_news_categories'.
  $permissions['edit terms in feds_news_categories'] = array(
    'name' => 'edit terms in feds_news_categories',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter feds_news revision log entry'.
  $permissions['enter feds_news revision log entry'] = array(
    'name' => 'enter feds_news revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_news authored by option'.
  $permissions['override feds_news authored by option'] = array(
    'name' => 'override feds_news authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_news authored on option'.
  $permissions['override feds_news authored on option'] = array(
    'name' => 'override feds_news authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_news promote to front page option'.
  $permissions['override feds_news promote to front page option'] = array(
    'name' => 'override feds_news promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_news published option'.
  $permissions['override feds_news published option'] = array(
    'name' => 'override feds_news published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_news revision option'.
  $permissions['override feds_news revision option'] = array(
    'name' => 'override feds_news revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_news sticky option'.
  $permissions['override feds_news sticky option'] = array(
    'name' => 'override feds_news sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_news content'.
  $permissions['search feds_news content'] = array(
    'name' => 'search feds_news content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
