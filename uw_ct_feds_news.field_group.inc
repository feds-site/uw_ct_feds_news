<?php

/**
 * @file
 * uw_ct_feds_news.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_feds_news_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_author_details|node|feds_news|default';
  $field_group->group_name = 'group_author_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_news';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Author\'s Details',
    'weight' => '9',
    'children' => array(
      0 => 'field_author_full_name',
      1 => 'field_author_title_position',
      2 => 'field_authors_photo',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-author-details field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_author_details|node|feds_news|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feds_news_author_details|node|feds_news|full';
  $field_group->group_name = 'group_feds_news_author_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_news';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'News Author Content',
    'weight' => '0',
    'children' => array(
      0 => 'field_feds_news_date',
      1 => 'field_author_full_name',
      2 => 'field_author_title_position',
      3 => 'field_authors_photo',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'News Author Content',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'author-detail-container',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_feds_news_author_details|node|feds_news|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feds_news_feature|node|feds_news|form';
  $field_group->group_name = 'group_feds_news_feature';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Feature Article',
    'weight' => '11',
    'children' => array(
      0 => 'field_feature_article',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-feds-news-feature field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_feds_news_feature|node|feds_news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feds_news_spotlight|node|feds_news|form';
  $field_group->group_name = 'group_feds_news_spotlight';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Spotlight Details',
    'weight' => '10',
    'children' => array(
      0 => 'field_spotlight_name',
      1 => 'field_spotlight_position',
      2 => 'field_content_is_a_spotlight',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-feds-news-spotlight field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_feds_news_spotlight|node|feds_news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feds_news_upload_file|node|feds_news|form';
  $field_group->group_name = 'group_feds_news_upload_file';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload a file',
    'weight' => '8',
    'children' => array(
      0 => 'field_choose_a_file',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-feds-news-upload-file field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_feds_news_upload_file|node|feds_news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feds_news_upload_image|node|feds_news|form';
  $field_group->group_name = 'group_feds_news_upload_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload an image',
    'weight' => '7',
    'children' => array(
      0 => 'field_image_to_body',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-feds-news-upload-image field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_feds_news_upload_image|node|feds_news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_news_authors|node|feds_news|form';
  $field_group->group_name = 'group_news_authors';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Author\'s Details',
    'weight' => '12',
    'children' => array(
      0 => 'field_author_full_name',
      1 => 'field_author_title_position',
      2 => 'field_authors_photo',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Author\'s Details',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-news-authors field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_news_authors|node|feds_news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_temporary_for_viewing_only|node|feds_news|form';
  $field_group->group_name = 'group_temporary_for_viewing_only';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Temporarily - Available for viewing only',
    'weight' => '18',
    'children' => array(
      0 => 'field_news_banner_image',
      1 => 'field_author_profile',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Temporarily - Available for viewing only',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-temporary-for-viewing-only field-group-fieldset',
        'description' => 'Used for legacy content. Not available for input. ',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_temporary_for_viewing_only|node|feds_news|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Author\'s Details');
  t('Feature Article');
  t('News Author Content');
  t('Spotlight Details');
  t('Temporarily - Available for viewing only');
  t('Upload a file');
  t('Upload an image');

  return $field_groups;
}
